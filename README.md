# Exercise 2.7 Intersection

Given two (singly) linked lists, determine if the two lists intersect. Return the intersecting node. Note that the intersection is defined based on reference, not value. That is, if the kth node of the first linked list is the exact same node (by reference) as the jth node of the second linked list, then they are intersecting.

_Hints_: #20, #45, #55, #65, #76, #93, #111, #120, #129

Whiteboard [analysis](whiteobard-images/analysis.png), [test cases](whiteboard-images/test-cases.png) and [code](whiteboard-images/code.png) are available using the links.

Questions, issues, comments, PRs all welcome.
