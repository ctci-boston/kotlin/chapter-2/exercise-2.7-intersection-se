import com.pajato.ctci.Node
import kotlin.math.abs

fun getIntersection(list1: Node<Char>?, list2: Node<Char>?): Node<Char>? {
    fun doesNotIntersect(): Boolean = list1 == null || list2 == null || list1.tail() != list2.tail()
    fun getIntersection(): Node<Char>? {
        tailrec fun getIntersection(longer: Node<Char>?, shorter: Node<Char>?, delta: Int): Node<Char>? {
            val nextLonger = longer?.next
            val nextShorter = if (delta == 0) shorter?.next else shorter

            if (delta == 0 && longer === shorter) return longer
            return getIntersection(nextLonger, nextShorter, if (delta > 0) delta - 1 else 0)
        }
        val length1 = list1?.length() ?: 0
        val length2 = list2?.length() ?: 0
        val delta = abs(length1 - length2)

        return if (length1 > length2) getIntersection(list1, list2, delta) else getIntersection(list2, list1, delta)
    }

    return if (doesNotIntersect()) null else getIntersection()
}